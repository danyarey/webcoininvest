from datetime import datetime
from pony.orm import *
from datetime import date

db = Database()
db.bind(provider='sqlite',filename='database.sqlite')
class Users(db.Entity):
    """users list"""
    i = Required(int)
    id = Required(int, unique=True)  # telegram id of user
    username = Optional(str)
    status = Optional(str)
    data = Optional(Json)
    bank = Optional('Bank')
    transaction = Set('Transaction')
    withdraws = Set('Withdraws')
    bans = Set('Bans')
    deposits = Set('Deposits')
    deposit_codes = Set('DepositCode')
    register_date = Optional(datetime, default=lambda: datetime.now())
    referals = Set('Users', reverse='referal')
    referal = Optional('Users', reverse='referals')
    PrimaryKey(i, id)


class Bank(db.Entity):
    """All about money"""
    i = PrimaryKey(int, auto=True)
    user = Required(Users)
    balance_deposited = Optional(float, default=0)
    balance_to_withdraw = Optional(float, default=0)
    speed = Optional(float, default=0)
    transaction = Set('Transaction')
    last_balance = Optional(float)
    last_check = Optional(datetime)
    data = Optional(str)


class Transaction(db.Entity):
    """Internal transactions, Withdraws and deposit"""
    i = PrimaryKey(int, auto=True)
    user = Required(Users)
    bank = Required(Bank)
    type = Optional(str)
    count = Optional(float)
    from_user = Optional(int)
    to_user = Optional(int)
    date = Optional(datetime, default=lambda: datetime.now())
    deposit = Optional('Deposits')
    withdraw = Required('Withdraws')


class Withdraws(db.Entity):
    i = PrimaryKey(int, auto=True)
    user = Required(Users)
    count = Optional(float)
    method = Optional(str)
    status = Optional(str, default='awaiting')
    comment = Optional(str)
    date = Optional(datetime, default=lambda: datetime.now())
    transaction = Optional(Transaction)


class Bans(db.Entity):
    i = PrimaryKey(int, auto=True)
    user = Required(Users)
    reason = Optional(str)
    actual = Optional(bool)
    unban_date = Optional(datetime)
    ban_date = Optional(datetime, default=lambda: datetime.now())


class Deposits(db.Entity):
    i = PrimaryKey(int, auto=True)
    user = Required(Users)
    count = Optional(float)
    source = Optional(str)
    status = Optional(str, default='pending')
    date = Optional(datetime, default=lambda: datetime.now())
    deposit_code = Optional('DepositCode')
    transaction = Optional(Transaction)


class DepositCode(db.Entity):
    i = PrimaryKey(int, auto=True)
    code = Optional(int, unique=True)
    user = Required(Users)
    deposit = Required(Deposits)
    date = Optional(datetime, default=lambda: datetime.now())


class System_vars(db.Entity):
    name = PrimaryKey(str, auto=True)
    val_str = Optional(str)
    val_float = Optional(float)
    val_json = Optional(Json)


class Coin_rate(db.Entity):
    date = PrimaryKey(date, default=lambda: date.today(), auto=True)
    rate = Optional(float)
    type = Optional(str)




db.generate_mapping()
# @db_session
# def test():
#     user = Users.get(i=1)
#     b = Bank(user=user,balance_deposited=1,bonus=1,balance_to_withdraw=1)
#     commit()
# test()