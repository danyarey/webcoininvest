from telegram import ReplyKeyboardMarkup, KeyboardButton
from utils.tools import dictgno


def markup_generator(markup, resize=True, ont=False, selective=False):
    ready_markup = []
    for line in markup:
        ready_line = []
        for button in line:

            if type(button) is dict:
                ready_button = KeyboardButton(text=button['text'],
                                              request_contact=dictgno(button)['contact'],
                                              request_location=dictgno(button)['loc'])
            elif type(button) is str:
                ready_button = button
            else:
                ready_button = ''
            ready_line.append(ready_button)
        ready_markup.append(ready_line)
    return ReplyKeyboardMarkup(keyboard=ready_markup,
                               resize_keyboard=resize,
                               one_time_keyboard=ont,
                               selective=selective)
