import db_structure
from utils.logger import log
import settings
from telegram import Bot
from telegram.ext import Updater
from time import sleep
from modules import handlers_generator
class Starter:
    def __init__(self):
        self.tg_token = settings.telegram_bot_token
        self.tg_bot = None
        self.tg_updater = None
        self.workers = settings.updater_workers

    def init_tg(self, token):
        bot = Bot(token=token)
        updater = Updater(bot=bot, workers=self.workers)
        self.tg_bot = bot
        self.tg_updater = updater

    def start_polling(self, updater: Updater, clean=False):
        bot = updater.bot
        if not clean:

            updates_count = bot.get_webhook_info().pending_update_count
            print(updates_count)
            if updates_count > settings.max_updates:
                clean = True
        status = updater.running
        if not status:
            result_of_start = updater.start_polling(clean=clean)
        log.info(str(result_of_start))
        status = updater.running
        return status

    def run(self):

        msg_for_log = 'Инициирован запуск бота, выполняется проверка: '
        # Проверяем условия запуска
        if settings.telegram_bot_token not in ['', None, False]:
            msg_for_log += '\nТокен установлен: ✅'
        else:
            msg_for_log += '\nТокен установлен: ❌'; return

        # TODO: Сделать диагностику системы оплаты
        if True:
            msg_for_log += '\nПлатежи работают: ✅'
        else:
            msg_for_log += '\nПлатежи работают: ❌'
            if not settings.ignore_payment_fault:
                log.info(msg_for_log)
                log.critical('Платежи не работают, запуск невозможен.')
                return
        # инициируем инстансы бота
        self.init_tg(self.tg_token)
        # TODO: сделать проверку на запуск от другого бота
        if self.tg_bot.name:
            msg_for_log += '\nБот запущен от аккаунта {} удачно : ✅'.format(self.tg_bot.name)
        else:
            msg_for_log += '\nБот запущен от аккаунта {} удачно: ❌'.format(self.tg_bot.name)
            log.info(msg_for_log)
            log.critical('Бот запущен не на том аккаунте или токен недействителен. Завершение работы')
            return

        if self.tg_updater:
            handlers_generator.genereate_handlers(self.tg_updater.dispatcher)
            polling_start_result = self.start_polling(self.tg_updater)
            if polling_start_result:
                msg_for_log += '\nБот запущен успешно!'
                log.info(msg_for_log)
        self.tg_updater.idle()



s = Starter()
s.run()
