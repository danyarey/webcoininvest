import logging
import settings
from requests import post
import sys
class TgFormatter(logging.Formatter):
    def __init__(self):
        super(TgFormatter, self).__init__()

    def format(self, record):
        type={logging.WARNING:'⚠️Warning⚠️',logging.DEBUG:'📝Debug📝',logging.INFO:'✅ Info ✅',logging.ERROR:'‼️Error‼️️',logging.CRITICAL:'⛔️!!!CRITICAL!!!⛔️'}
        return "<b>{type}</b>\n<code>{message}</code>".format(message=record.msg, type=type[record.levelno])
class RequestsHandler(logging.Handler):
    def emit(self, record):
        log_entry = self.format(record)
        payload = {
            'chat_id': settings.channel_for_logs,
            'text': log_entry,
            'parse_mode': 'HTML'
        }
        return post("https://api.telegram.org/bot{token}/sendMessage".format(token=settings.telegram_bot_token),
                             data=payload).content


log = logging.getLogger('bot')
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] %(levelname)s  %(message)s')
tgformatter = TgFormatter()
filegandler = logging.FileHandler('bot.log','w', 'utf-8')
filegandler.setLevel(logging.DEBUG)
filegandler.setFormatter(formatter)

tghandler = RequestsHandler()
tghandler.setFormatter(tgformatter)
tghandler.setLevel(logging.INFO)

streamhandler = logging.StreamHandler(sys.stdout)
streamhandler.setFormatter(formatter)
streamhandler.setLevel(logging.DEBUG)
log.addHandler(tghandler)
log.addHandler(filegandler)
log.addHandler(streamhandler)